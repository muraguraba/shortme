﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.Users;
using WebApi.Services;

namespace WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class LinksController : ControllerBase
    {
        private IUserService _userService;
        private IMapper _mapper;

        public LinksController(
            IUserService userService,
            IMapper mapper)
        {
            _userService = userService;
            _mapper = mapper;
        }
        [HttpGet]
        public IActionResult GetAll()
        {
            var user = User.Identity.Name;
            var links = _userService.GetAllLinks(user);
            foreach (var v in links)
            {
                v.ShortUrl = $"<a href='links/{v.ShortUrl}'>{v.ShortUrl}</a>";
            }
            links = links.OrderByDescending(a => a.Id).ToList();
            return Ok(links);
        }
        [AllowAnonymous]
        [HttpGet("Short.me/{path}")]
        public IActionResult Short(string path)
        {
            var id = ShortLink.GetId(path);
            var link = _userService.GetLinkById(id);
            return Redirect(link.Url);
        }
        [HttpPost("shortcut")]
        public IActionResult Shortcut([FromBody] LinkModel model)
        {
            var user = User.Identity.Name;
            var url = model.Url;

            var link = new Link
            {
                Url = url,
                UserId = Convert.ToInt32(user)
            };
            try
            {
                _userService.Create(link);
                var urlChunk = $"Short.me/{ShortLink.GetUrlChunk(link.Id)}";
                link.ShortUrl = urlChunk;
                _userService.Update(link);
                return Ok(urlChunk);
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }
    }
}
