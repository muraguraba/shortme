$('#idLogin').click(function (event) {
    event.preventDefault();
    let model = {
        Username: document.getElementById("inputEmailAddress").value,
        Password: document.getElementById("inputPassword").value
    };
    $.ajax({
        type: "POST",
        url: "/users/authenticate",
        contentType: "application/json",
        dataType: 'json',
        data: JSON.stringify(model),
        complete: function (result) {
            console.log(result.responseText);
            if (result.status == 400) {
                alert(result.responseText);
            }
            if (result.status == 200) {
                localStorage.token = result.responseJSON.token;
                window.location.href = "index.html";        
            }
        }
    });
});
$('#idRegister').click(function (event) {
    event.preventDefault();
    let model = {
        Username: document.getElementById("inputEmailAddress").value,
        Password: document.getElementById("inputPassword").value
    };
    $.ajax({
        type: "POST",
        url: "/users/register",
        contentType: "application/json",
        dataType: 'json',
        data: JSON.stringify(model),
        complete: function (result) {
            console.log(result.responseText);
            if (result.status == 400) {
                alert(result.responseText);
            }
            if (result.status == 200) {
                alert("Success!");
                window.location.href = "login.html";    
            }
        }
    });
});
$('#idShortcut').click(function (event) {
    event.preventDefault();
    let model = {
        Url: document.getElementById("url").value
    };
    $.ajax({
        type: "POST",
        url: "/links/shortcut",
        beforeSend: function (xhr) {
            if (localStorage.token) {
                xhr.setRequestHeader('Authorization', 'Bearer ' + localStorage.token);
            }
        },
        contentType: "application/json",
        dataType: 'json',
        data: JSON.stringify(model),
        complete: function (result) {
            console.log(result.responseText);
            if (result.status == 400) {
                alert(result.responseText);
            }
            if (result.status == 200) {
               // alert("Success!");
                let url = result.responseText;
                let section = document.getElementById('urlResult');
                section.innerHTML = `<a id="link" href="links/${url}">${url}</a>`;

                getLinks();
            }
        }
    });
});

function getLinks() {
    $.ajax({
        type: 'GET',
        url: 'links',
        beforeSend: function (xhr) {
            if (localStorage.token) {
                xhr.setRequestHeader('Authorization', 'Bearer ' + localStorage.token);
            }
        },
        success: function (data) {
            console.log(data);
            $('#idtable').DataTable({
                "data": data,
                destroy: true,
                ordering: false,
                columns: [
                    { "data": "url" },
                    { "data": "shortUrl" }
                ]
            });
        },
        error: function (data) {
            console.log(data);
            window.location.href = "login.html";
        },
        complete: function (result) {
            console.log(result);
        }
    });
}
$(document).ready(function () {
    if (window.location.pathname == "/index.html" || window.location.pathname == "/") {
        getLinks();
    }

});