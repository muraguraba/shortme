
Web application built with ASP.NET Core 3.1

Running the ASP.NET Core Locally
Download or clone the project code from https://gitlab.com/muraguraba/shortme.git
Start the api by running dotnet run from the command line in the project root folder, you should see the message Now listening on: http://localhost:4000.